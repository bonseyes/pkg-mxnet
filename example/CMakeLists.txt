add_executable(fd_test face-recognition-distance.cc)

target_link_libraries(fd_test ${MXNET_LIBRARIES} ${NCV_LIBRARIES})

target_include_directories(fd_test
    PUBLIC ${JSON_INCLUDE_DIRS}
    PUBLIC ${PLOG_INCLUDE_DIRS}
    PUBLIC ${CMAKE_SOURCE_DIR}/sdk/utils/inc
    PRIVATE ${CMAKE_BINARY_DIR}/deps/include
    PRIVATE ${CMAKE_SOURCE_DIR}/sdk/inc
    )

add_dependencies(fd_test mxnet_project)

install(TARGETS fd_test RUNTIME DESTINATION bin)

install(FILES
  ${CMAKE_CURRENT_SOURCE_DIR}/model-mobilefacenet-arc-0062.params
  ${CMAKE_CURRENT_SOURCE_DIR}/model-mobilefacenet-arc-symbol.json
  ${CMAKE_CURRENT_SOURCE_DIR}/Jennifer_Aniston_0001.jpg
  ${CMAKE_CURRENT_SOURCE_DIR}/Jennifer_Aniston_0003.jpg
  ${CMAKE_CURRENT_SOURCE_DIR}/test.sh
  DESTINATION bin)
