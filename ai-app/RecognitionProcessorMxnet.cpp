/**
* NVISO CONFIDENTIAL
*
* Copyright (c) 2009- 2014 nViso SA. All Rights Reserved.
*
* The source code contained or described herein and all documents related to
* the source code ("Material") is the confidential and proprietary information
* owned by nViso or its suppliers or licensors.  Title to the  Material remains
* with nViso Sarl or its suppliers and licensors. The Material contains trade
* secrets and proprietary and confidential information of nViso or its suppliers
* and licensors. The Material is protected by worldwide copyright and trade
* secret laws and treaty provisions. You shall not disclose such Confidential
* Information and shall use it only in accordance with the terms of the license
* agreement you entered into with nViso.
*
* NVISO MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
* THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
* TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
* PARTICULAR PURPOSE, OR NON-INFRINGEMENT. NVISO SHALL NOT BE LIABLE FOR
* ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR
* DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
*/

#include "RecognitionProcessorMxnet.hpp"

#include "nviso_utils.hpp"

#include <nvisocv/Mat.hpp>
#include <nvisocv/ImageTile.hpp>
#include <nvisocv/imgcodecs.hpp>
#include <nvisocv/imgproc.hpp>

#include <mxnet/c_predict_api.h>

#include <algorithm>
#include <cstring>
#include <iostream>
#include <string>

using namespace nviso;
using namespace std;

namespace nviso {

struct RecognitionProcessorMxnet::Private {
  Private() {}

  /// model params
  string model_;

  /// model params
  string weight_;

  /// Config date for each model type
  json modelConfig_;

  ///
  PredictorHandle hdl_{nullptr};
};

// Read file to buffer
class BufferFile {
 public :
  std::string file_path_;
  std::size_t length_ = 0;
  std::unique_ptr<char[]> buffer_;

  explicit BufferFile(const std::string& file_path)
    : file_path_(file_path) {
    if (file_path.empty()) {
        LOGE << "empty file provided";
        return;
    }

    LOGV << "Reading file " << file_path;

    std::ifstream ifs(file_path.c_str(), std::ios::in | std::ios::binary);
    if (!ifs) {
      std::cerr << "Can't open the file. Please check " << file_path << ". \n";
      return;
    }

    ifs.seekg(0, std::ios::end);
    length_ = static_cast<std::size_t>(ifs.tellg());
    ifs.seekg(0, std::ios::beg);
    LOGV << file_path.c_str() << " ... " << length_ << " bytes\n";

    buffer_.reset(new char[length_]);
    ifs.read(buffer_.get(), length_);
    ifs.close();
  }

  std::size_t GetLength() {
    return length_;
  }

  const char* GetBuffer() const {
    return buffer_.get();
  }
};

RecognitionProcessorMxnet::RecognitionProcessorMxnet() : d(new Private) {}

RecognitionProcessorMxnet::~RecognitionProcessorMxnet() { delete d; }

bool RecognitionProcessorMxnet::init(const json& cfg) {
  if (!setValue(d->model_, cfg, "model")) {
      LOGE << " No model specified";
      return false;
  }
  if (!setValue(d->weight_, cfg, "weights")) {
      LOGE << " No model weight specified";
      return false;
  }
  return true;
}

// Image size and channels
static const mx_uint width = 112;
static const mx_uint height = 112;
static const mx_uint channels = 3;

static auto imageSize = static_cast<std::size_t>(width * height * channels);


bool RecognitionProcessorMxnet::setOptions(const json& cfg) {
  if (d->hdl_ != nullptr) {
      LOGV << "already done";
      return true;
  }

  BufferFile json_data(d->model_);
  if (json_data.GetLength() == 0) {
      LOGE << "problem with model params " << d->model_;
      return false;
  }
  BufferFile param_data(d->weight_);
  if (param_data.GetLength() == 0) {
      LOGE << "problem with model weight" << d->weight_;
      return false;
  }

  // Parameters
  const int dev_type = 1;  // 1: cpu, 2: gpu
  const int dev_id = 0;  // arbitrary.
  const mx_uint num_input_nodes = 1;  // 1 for feedforward
  const char* input_key[1] = { "data" };
  const char** input_keys = input_key;

  const mx_uint input_shape_indptr[2] = { 0, 4 };
  const mx_uint input_shape_data[4] = { 1, channels, height, width};

  // Create Predictor
  const int res = MXPredCreate(
              json_data.GetBuffer(),
              param_data.GetBuffer(),
              static_cast<int>(param_data.GetLength()),
               dev_type,
               dev_id,
               num_input_nodes,
               input_keys,
               input_shape_indptr,
               input_shape_data,
               &d->hdl_);

  if (res) {
      LOGE << "MXPredCreate failed with code " << res;
      return false;
  }
  if (d->hdl_ == nullptr) {
      LOGE << "MXPredCreate failed";
      return false;
  }
  return true;
}

bool RecognitionProcessorMxnet::predict(const ncv::ImageTile& img,
                                        FaceDescriptor* recognition) {

    if (img.c != channels || img.w != width || img.h != height) {
      LOGE << "wrong image size: " << img.c << "x" << img.h << "x" << img.w
           << " expected: " << channels << "x" << height << "x" << width;
      return false;
    }

    Timer t;

    // Set Input Image
    int res = MXPredSetInput(d->hdl_, "data", img.data, imageSize);
    if (res) {
        LOGE << "MXPredSetInput failed with code " << res;
        return false;
    }

    // Do Predict Forward
    res = MXPredForward(d->hdl_);
    if (res) {
        LOGE << "MXPredForward failed with code " << res;
        return false;
    }

    mx_uint output_index = 0;

    mx_uint* shape = nullptr;
    mx_uint shape_len;

    // Get Output Result
    res = MXPredGetOutputShape(d->hdl_, output_index, &shape, &shape_len);
    if (res) {
        LOGE << "MXPredGetOutputShape failed with code " << res;
        return false;
    }

    std::size_t size = 1;
    for (mx_uint i = 0; i < shape_len; ++i) { size *= shape[i]; }
    recognition->resize(size);

    t.tic();

    res = MXPredGetOutput(d->hdl_, output_index, &((*recognition)[0]), static_cast<mx_uint>(size));
    if (res) {
        LOGE << "MXPredGetOutput failed with code " << res;
        return false;
    }

    LOGV << "inference time " << t.toc();

    return true;
}

}
